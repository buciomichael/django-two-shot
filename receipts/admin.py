from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account


admin.site.register(ExpenseCategory)
admin.site.register(Receipt)
admin.site.register(Account)
